import java.io.*;
import java.util.ArrayList;

public class ViewResult implements View{

    private static final String FNAME = "save.bin";
    private static final int DEFAULT_NUM = 10;
    private ArrayList<Item> items = new ArrayList<Item>();

    public ViewResult() {
        this(DEFAULT_NUM);
    }
    public ViewResult(int n) {
        for(int ctr = 0; ctr < n; ctr++) {
            items.add(new Item());
        }
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    /**
     * Метод calc приймає десяткове число та повертає кількість чергувань бітів 0 і 1 в його двійковому представленні.
     *
     * @param n десяткове число, для якого необхідно обчислити кількість чергувань бітів 0 і 1
     * @return кількість чергувань бітів 0 і 1 у двійковому представленні числа n
     */
    public int calc(int n) {
        String binaryString = Integer.toBinaryString(n);

        int count = 0;
        char prevBit = binaryString.charAt(0);

        for(int i = 1; i < binaryString.length(); i++) {
            char currBit = binaryString.charAt(i);
            if (currBit != prevBit) {
                count++;
            }
            prevBit = currBit;
        }

        return count;
    }
    public void init(int step) {
        int i = 0;
        for (Item item : items) {
            item.setResult(calc(i));
            i += step;
        }
    }
    
    @Override
    public void viewHeader() {
        System.out.println("Results: ");
    }

    @Override
    public void viewBody() {
        for(Item item : items) {
            System.out.printf(Long.toString(item.getResult()));
        }
        System.out.println();
    }

    @Override
    public void viewFooter() {
        System.out.println("End");
    }

    @Override
    public void viewShow() {
        viewHeader();
        viewBody();
        viewFooter();
    }

    @Override
    public void viewInit() {
        init((int) (Math.random() * 360));
    }

    @Override
    public void viewSave() throws IOException {
        ObjectOutputStream os = new ObjectOutputStream(new
                FileOutputStream(FNAME));
        os.writeObject(items);
        os.flush();
        os.close();
    }

    @Override
    public void viewRestore() throws Exception {
        ObjectInputStream is = new ObjectInputStream(new FileInputStream(FNAME));
        items = (ArrayList<Item>) is.readObject();
        is.close();
    }
}

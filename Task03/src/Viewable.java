public interface Viewable {
    public View getView();
}

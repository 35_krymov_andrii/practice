import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MainTest {

    private static final String FNAME = "save.bin";
    private ViewResult viewResult;

    @BeforeEach
    public void setUp() {
        viewResult = new ViewResult();
    }

    @AfterEach
    public void tearDown() {
        File file = new File(FNAME);
        if (file.exists()) {
            file.delete();
        }
    }

    @Test
    public void testCalc() {
        int result = viewResult.calc(5);
        assertEquals(2, result);
    }

    @Test
    public void testGetItems() {
        assertNotNull(viewResult.getItems());
    }

    @Test
    public void testInit() {
        viewResult.init(2);
        assertEquals(0, viewResult.getItems().get(0).getResult());
        assertEquals(1, viewResult.getItems().get(1).getResult());
    }

    @Test
    public void testViewSaveAndRestore() throws IOException, Exception {
        viewResult.init(1);
        viewResult.viewSave();

        ViewResult restoredViewResult = new ViewResult();
        restoredViewResult.viewRestore();

        assertEquals(viewResult.getItems().size(), restoredViewResult.getItems().size());
        for (int i = 0; i < viewResult.getItems().size(); i++) {
            assertEquals(viewResult.getItems().get(i).getResult(),
                    restoredViewResult.getItems().get(i).getResult());
        }
    }
}
package ex5;

import ex4.View;
/** Консольная команда
 * View;
 * шаблон ex5.Command
 * @author xone
 * @version 1.0
 */
public class ViewConsoleCommand implements ConsoleCommand {
    /** Объект, реализующий интерфейс {@linkplain View};
     * обслуживает коллекцию объектов {@linkplain ex4.Item}
     */
    private View view;

    public ViewConsoleCommand(View view) {
        this.view = view;
    }
    @Override
    public char getKey() {
        return 'v';
    }
    @Override
    public String toString() {
        return "'v'iew";
    }
    @Override
    public void execute() {
        System.out.println("View current.");
        view.viewShow();
    }
}
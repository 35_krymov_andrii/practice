package ex5;

/** Интерфейс
 * консольной команды;
 * шаблон ex5.Command
 * @author xone
 * @version 1.0
 */
public interface ConsoleCommand extends Command {
    /** Горячая клавиша команды;
     * шаблон ex5.Command
     * @return символ горячей клавиши
     */
    public char getKey();
}
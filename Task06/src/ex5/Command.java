package ex5;

/** Интерфейс команды
 * или задачи;
 * шаблоны: ex5.Command,
 * Worker Thread
 * @author xone
 * @version 1.0
 */
public interface Command {
    /** Выполнение команды; шаблоны: ex5.Command, Worker Thread */
    public void execute();
}
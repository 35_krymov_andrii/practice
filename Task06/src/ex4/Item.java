package ex4;

import java.io.Serializable;

public class Item implements Serializable {

    private int result;

    private int number;

    public int getResult() {
        return result;
    }
    public void setResult(int result) {
        this.result = result;
    }
    public int setNumber(int number){
        return this.number = number;
    }
    public int getNumber(){
        return number;
    }
}

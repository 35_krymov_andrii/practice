package ex4;

public interface Viewable {
    public View getView();
}

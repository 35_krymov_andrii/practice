import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Main клас включає в себе головне меню програми
 */
public class Main {
    private Calc calc = new Calc();
    private Item item = new Item();
    /** Меню */
    private void menu() throws IOException {
        String s = null;
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        do {
            do {
                System.out.println("Enter command...");
                System.out.print("'q'uit, 'v'iew, 'g'enerate, 's'ave, 'r'estore: ");
                try {
                    s = in.readLine();
                } catch(IOException e) {
                    System.out.println("Error: " + e);
                    System.exit(0);
                }
            } while (s.length() != 1);
            switch (s.charAt(0)) {
                case 'q' -> System.out.println("Exit.");
                case 'v' -> System.out.println(item.getResult());
                case 'g' -> {
                    System.out.println("Random generation.");
                    System.out.println("Enter number:");
                    s = in.readLine();
                    item.generate(Integer.parseInt(s));
                    System.out.println(item.getResult());
                }
                case 's' -> {
                    System.out.println("Save current.");
                    try {
                        System.out.println("Enter number:");
                        s = in.readLine();
                        calc.save(Integer.parseInt(s));
                        System.out.println("Saved: " + item.getResult());
                    } catch (IOException e) {
                        System.out.println("Serialization error: " + e);
                    }
                }
                case 'r' -> {
                    System.out.println("Restore last saved.");
                    try {
                        calc.restore();
                    } catch (Exception e) {
                        System.out.println("Serialization error: " + e);
                    }
                }
                default -> System.out.print("Wrong command. ");
            }
        } while(s.charAt(0) != 'q');
    }

    public static void main(String[] args) throws IOException {
        Main main = new Main();
        main.menu();
    }

}

import java.io.*;

/**
*Клас Calc містить метод calc, який обчислює кількість чергувань бітів 0 і 1
*у двійковому представленні заданого десяткового числа.
 */
public class Calc {
    private static final String FNAME = "save.bin";

    private Item item = new Item();

    /**
     * Серіалізація
     * @param n
     * @throws IOException
     */
    public void save(int n) throws IOException {
        ObjectOutputStream os = new ObjectOutputStream(new
                FileOutputStream(FNAME));
        item.generate(n);
        os.writeObject(item);
        os.flush();
        os.close();
    }

    /**
     * Десеріалізація
     * @throws Exception
     */
    public void restore() throws Exception {
        ObjectInputStream is = new ObjectInputStream(new FileInputStream(FNAME));
        item = (Item) is.readObject();
        System.out.println(item.getResult());
        is.close();
    }
}

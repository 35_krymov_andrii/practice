import java.io.Serializable;

public class Item implements Serializable {

    private int result;

    /**
     * Метод calc приймає десяткове число та повертає кількість чергувань бітів 0 і 1 в його двійковому представленні.
     *
     * @param n десяткове число, для якого необхідно обчислити кількість чергувань бітів 0 і 1
     * @return кількість чергувань бітів 0 і 1 у двійковому представленні числа n
     */
    public void calc(int n) {
        n = Integer.parseInt(Integer.toBinaryString(n));

        int count = 0;
        long prevBit = n & 1;
        n = n >> 1;

        while (n != 0) {
            long currBit = n & 1;
            if (currBit != prevBit) {
                count++;
            }
            prevBit = currBit;
            n = n >> 1;
        }
        this.result = count;
    }
    public void generate(int num){
        calc(num);
    }
    public long getResult() {
        return result;
    }
}

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import junit.framework.Assert;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

/** Выполняет тестирование разработанных классов.
 * @author xone
 * @version 1.0
 */
public class MainTest {
    /** Проверка основной функциональности класса {@linkplain Calc} */
    @Test
    public void testCalc() throws IOException {
        Calc calc = new Calc();
        calc.save(5);
    }
    /** Проверка сериализации. Корректность восстановления данных. */
    @Test
    public void testRestore() throws IOException, ClassNotFoundException {
        final String FNAME = "save.bin";
        Item item = new Item();
        ObjectInputStream is = new ObjectInputStream(new FileInputStream(FNAME));
        item = (Item) is.readObject();
        System.out.println(item.getResult());
        is.close();
    }
}
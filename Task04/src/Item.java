import java.io.Serializable;

public class Item implements Serializable {

    private long result;

    private int number;

    public long getResult() {
        return result;
    }
    public void setResult(long result) {
        this.result = result;
    }
    public int setNumber(int number){
        return this.number = number;
    }
    public int getNumber(){
        return number;
    }
}

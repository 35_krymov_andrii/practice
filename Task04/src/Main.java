import java.io.IOException;

public class Main extends Main03 {

    public Main(View view) {
        super(view);
    }
    /** Выполняется при запуске программы;
     * вызывает метод {@linkplain Main03#menu menu()}
     * @param args - параметры запуска программы
     */
    public static void main(String[] args) throws IOException {
        Main main = new Main(new ViewableTable().getView());
        main.menu();
    }
}
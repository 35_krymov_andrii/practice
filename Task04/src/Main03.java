import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * ex4.ex5.Main клас включає в себе головне меню програми
 */
public class Main03 {
    private final View view;
    /** Инициализирует поле {@linkplain Main03#view view}. */
    public Main03(View view) {
        this.view = view;
    }

    /** Меню */
    void menu() throws IOException {
        String s = null;
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        do {
            do {
                System.out.println("Enter command...");
                System.out.print("'q'uit, 'v'iew, 'g'enerate, 's'ave, 'r'estore: ");
                try {
                    s = in.readLine();
                } catch(IOException e) {
                    System.out.println("Error: " + e);
                    System.exit(0);
                }
            } while (s.length() != 1);
            switch (s.charAt(0)) {
                case 'q' -> System.out.println("Exit.");
                case 'v' -> {
                    System.out.println("ex4.View current.");
                    view.viewShow();
                }
                case 'g' -> {
                    System.out.println("Random generation.");
                    view.viewInit();
                    view.viewShow();
                }
                case 's' -> {
                    System.out.println("Save current.");
                    try {
                        view.viewSave();
                    } catch (IOException e) {
                        System.out.println("Serialization error: " + e);
                    }
                }
                case 'r' -> {
                    System.out.println("Restore last saved.");
                    try {
                        view.viewRestore();
                    } catch (Exception e) {
                        System.out.println("Serialization error: " + e);
                    }
                    view.viewShow();
                }
                default -> System.out.print("Wrong command. ");
            }
        } while(s.charAt(0) != 'q');
    }

    public static void main(String[] args) throws IOException {
        Main03 main = new Main03(new ViewableResult().getView());
        main.menu();
    }

}

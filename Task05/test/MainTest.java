import static org.junit.Assert.*;
import org.junit.Test;
import ex4.Item;
import ex4.ViewResult;
/** Тестирование класса
 * ex5.ChangeItemCommand
 * @author xone
 * @version 4.0
 * @see ChangeItemCommand
 */
public class MainTest {
    /** Проверка метода {@linkplain ChangeItemCommand#execute()} */
    @Test
    public void testExecute() {
        ChangeItemCommand cmd = new ChangeItemCommand();
        cmd.setItem(new Item());
        for (int ctr = 0; ctr < 1000; ctr++) {
            cmd.execute();
        }
    }

    /** Проверка класса {@linkplain ChangeConsoleCommand} */
    @Test
    public void testChangeConsoleCommand() {
        ChangeConsoleCommand cmd = new ChangeConsoleCommand(new ViewResult() {
            @Override
            public void init(double step) {

            }
        });
        cmd.getView().viewInit();
        cmd.execute();
        assertEquals("'c'hange", cmd.toString());
        assertEquals('c', cmd.getKey());
    }
}